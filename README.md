## Overview

After cloning this project, you can run the following to link these dotfiles
into your home directory:

    ./activate.sh

Be warned: this will overwrite any existing .vimrc, .gvimrc or .vim/ files you
have in your home directory.

Uses `vim-plug` to manage bundles.
